:COMMBLOG: https://communityblog.fedoraproject.org
:MAILING-LIST: https://discussion.fedoraproject.org/tag/docs
:FEDMSG: http://www.fedmsg.com/
:FWIKI: https://fedoraproject.org/wiki
:MATRIX: https://matrix.to/#/#docs:fedoraproject.org
:YEAR: 2022
